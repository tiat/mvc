<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Mvc
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Mvc\Controller;

//
use ReflectionClass;
use SplFileInfo;
use Tiat\Mvc\Exception\BadMethodCallException;
use Tiat\Mvc\Exception\DomainException;
use Tiat\Mvc\Helper\DispatcherHelperTrait;
use Tiat\Mvc\Helper\HeadersHelperTrait;
use Tiat\Mvc\Helper\HeadersHelperTraitInterface;
use Tiat\Router\Response\ResponseContent;
use Tiat\Router\Response\ResponseContentInterface;
use Tiat\Standard\Parameters\ParametersPluginInterface;
use Tiat\Stdlib\Parameters\ParametersPlugin;

use function method_exists;
use function sprintf;
use function str_replace;

/**
 * Application controller should extend this class & this class is controlled by AbstractController.
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
abstract class AbstractActionController implements ControllerInterface, HeadersHelperTraitInterface, ParametersPluginInterface, ResponseContentInterface {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const string DEFAULT_TEMPLATE_DIR_NAME = 'Template';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	use DispatcherHelperTrait;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	use HeadersHelperTrait;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	use ResponseContent;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	use ParametersPlugin;
	
	/**
	 * @var string
	 * @since   3.0.0 First time introduced.
	 */
	private string $_pathModule;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	public function __construct() {
		//
		$this->setModulePath(str_replace('Controller', '',
		                                 ( new SplFileInfo(( new ReflectionClass($this) )->getFileName()) )->getPath()));
	}
	
	/**
	 * @param    string    $path
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setModulePath(string $path) : static {
		//
		$this->_pathModule = $path;
		
		//
		return $this;
	}
	
	/**
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function getModulePath() : string {
		return $this->_pathModule;
	}
	
	/**
	 * @param    string    $action
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function onDispatch(string $action) : void {
		//
		$this->dispatchStart();
		$this->preDispatch();
		
		// Dispatch the requested action
		$this->_onDispatch($action);
		
		//
		$this->postDispatch();
		$this->dispatchEnd();
	}
	
	/**
	 * @param    string    $action
	 *
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	private function _onDispatch(string $action) : mixed {
		//
		if(method_exists($this, $action) || method_exists($this, '__call')):
			return $this->$action();
		else:
			$msg = sprintf("Method %s doesn't exist.", $action);
			throw new BadMethodCallException($msg);
		endif;
	}
	
	/**
	 * @param    string    $method
	 * @param    array     $args
	 *
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	public function __call(string $method, array $args) {
		$msg = sprintf("Method '%s' doesn't exists.", $method);
		throw new DomainException($msg);
	}
	
	/**
	 * Prevent cloning
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	final protected function __clone() : void {
	
	}
}
