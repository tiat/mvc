<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Mvc
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Mvc\Controller;

/**
 * AbstractController is used to call application controller and run it.
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
abstract class AbstractController {

}
