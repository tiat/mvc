<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Mvc
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Mvc\Controller;

//
use Tiat\Mvc\Helper\DispatcherHelperTraitInterface;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface ControllerInterface extends DispatcherHelperTraitInterface {
	
	/**
	 * @param    string    $action
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function onDispatch(string $action) : void;
	
	/**
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function getModulePath() : string;
}
