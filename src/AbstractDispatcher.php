<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Mvc
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Mvc;

//
use Tiat\Mvc\Controller\ControllerInterface;
use Tiat\Mvc\Exception\RuntimeException;
use Tiat\Mvc\Helper\DispatcherHelperTrait;
use Tiat\Standard\Loader\ClassLoaderInterface;
use Tiat\Stdlib\Loader\ClassLoaderTrait;

use function register_shutdown_function;

/**
 * Dispatcher is used to launch controller & it's methods for response.
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
abstract class AbstractDispatcher implements ClassLoaderInterface, DispatcherInterface {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	use ClassLoaderTrait;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	use DispatcherHelperTrait;
	
	/**
	 * @var string
	 * @since   3.0.0 First time introduced.
	 */
	private string $_controllerAction;
	
	/**
	 * @param    DispatchManagerInterface    $_manager
	 * @param    ControllerInterface         $_controller
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public function __construct(protected readonly DispatchManagerInterface $_manager, protected readonly ControllerInterface $_controller) {
		register_shutdown_function([$this, 'shutdown']);
	}
	
	/**
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getControllerAction() : ?string {
		return $this->_controllerAction ?? NULL;
	}
	
	/**
	 * @param    string    $action
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setControllerAction(string $action) : static {
		//
		$this->_controllerAction = $action;
		
		//
		return $this;
	}
	
	/**
	 * @return never
	 * @since   3.0.0 First time introduced.
	 */
	final public function __invoke() : never {
		throw new RuntimeException("Using as callable is not possible.");
	}
	
	/**
	 * @param ...$args
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	abstract public function shutdown(...$args) : void;
}
