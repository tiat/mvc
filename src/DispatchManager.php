<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Mvc
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Mvc;

//

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class DispatchManager extends AbstractDispatchManager {
	
	/**
	 * @return $this
	 */
	public function createDispatcher() : static {
		if(( ( $plugin = $this->checkApplicationDispatcherPlugin() ) !== NULL ) && class_exists($plugin, TRUE)):
			$this->_setDispatcherInterface(new $plugin($this, $this->getControllerInterface()));
		else:
			$this->_setDispatcherInterface(new Dispatcher($this, $this->getControllerInterface()));
		endif;
		
		//
		return $this;
	}
}
