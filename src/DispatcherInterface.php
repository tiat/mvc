<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Mvc
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Mvc;

//
use Tiat\Mvc\Controller\ControllerInterface;
use Tiat\Mvc\Helper\DispatcherHelperTraitInterface;
use Tiat\Router\Response\ResponseContentInterface;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface DispatcherInterface extends DispatcherHelperTraitInterface {
	
	/**
	 * @param    DispatchManagerInterface    $_manager
	 * @param    ControllerInterface         $_controller
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public function __construct(DispatchManagerInterface $_manager, ControllerInterface $_controller);
	
	/**
	 * @param ...$args
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function shutdown(...$args) : void;
	
	/**
	 * @return null|(ControllerInterface&ResponseContentInterface)
	 * @since   3.0.0 First time introduced.
	 */
	public function getControllerInterface() : null|( ControllerInterface&ResponseContentInterface );
	
	/**
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getControllerAction() : ?string;
	
	/**
	 * @param    string    $action
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setControllerAction(string $action) : DispatcherInterface;
}
