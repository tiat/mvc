<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Mvc
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Mvc;

//
use Tiat\Mvc\Controller\ControllerInterface;
use Tiat\Mvc\Exception\DomainException;
use Tiat\Mvc\Exception\RuntimeException;
use Tiat\Router\Response\ResponseContentInterface;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class Dispatcher extends AbstractDispatcher {
	
	/**
	 * @param    mixed    ...$args
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function run(...$args) : static {
		//
		if($this->isInitCompleted() === FALSE):
			$this->init();
		endif;
		
		//
		if($this->isInitCompleted() === TRUE && $this->getControllerAction() !== NULL):
			// Before the application controller
			$this->dispatchStart();
			$this->preDispatch();
			
			// Move control for Application controller system
			if(( $c = $this->getControllerInterface() ) !== NULL):
				$c->onDispatch($this->getControllerAction());
			else:
				// This should be impossible if __construct() is working.
				throw new RuntimeException("No controller interface declared.");
			endif;
			
			// After the application controller
			$this->postDispatch();
			$this->dispatchEnd();
		else:
			throw new DomainException("There is something wrong in application and execution has been stopped.");
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return null|ResponseContentInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getControllerInterface() : null|( ControllerInterface&ResponseContentInterface ) {
		return $this->_controller ?? NULL;
	}
	
	/**
	 * @param ...$args
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function shutdown(...$args) : void {
	
	}
}
