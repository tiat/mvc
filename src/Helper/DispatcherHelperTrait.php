<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Mvc
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Mvc\Helper;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
trait DispatcherHelperTrait {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	public const string SERVICE_TEMPLATE = 'service-name';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	public const string TEMPLATE_KEYS = 'ViewModel';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	public const string SERVICE_TEMPLATE_KEY_BODY = 'body';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	public const string SERVICE_TEMPLATE_KEY_HEAD = 'head';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	public const string SERVICE_TEMPLATE_KEY_HEADER = 'header';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	public const string SERVICE_TEMPLATE_KEY_FOOTER = 'footer';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	public const string SERVICE_TEMPLATE_KEY_RENDER = 'render';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	public const string SERVICE_TEMPLATE_KEY_RENDERER = 'renderer';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	public const string SERVICE_TEMPLATE_KEY_RESOLVER = 'resolver';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	public const string SERVICE_TEMPLATE_KEY_VIEW = 'view';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	public const array SERVICE_TEMPLATE_KEYS = [self::SERVICE_TEMPLATE_KEY_BODY, self::SERVICE_TEMPLATE_KEY_HEAD,
	                                            self::SERVICE_TEMPLATE_KEY_HEADER, self::SERVICE_TEMPLATE_KEY_FOOTER,
	                                            self::SERVICE_TEMPLATE_KEY_RENDER, self::SERVICE_TEMPLATE_KEY_RENDERER,
	                                            self::SERVICE_TEMPLATE_KEY_RESOLVER, self::SERVICE_TEMPLATE_KEY_VIEW];
	
	/**
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function dispatchStart() : void {
	
	}
	
	/**
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function preDispatch() : void {
	
	}
	
	/**
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function postDispatch() : void {
	
	}
	
	/**
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function dispatchEnd() : void {
	
	}
}
