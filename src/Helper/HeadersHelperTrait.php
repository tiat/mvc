<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Mvc
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Mvc\Helper;

use Tiat\Mvc\Exception\InvalidArgumentException;

use function sprintf;

/**
 * Provides helper methods for dealing with headers.
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
trait HeadersHelperTrait {
	
	/**
	 * @var array
	 * @since   3.0.0 First time introduced.
	 */
	private array $_headers;
	
	/**
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function getHeaders() : ?array {
		return $this->_headers ?? NULL;
	}
	
	/**
	 * @param    array    $headers
	 * @param    bool     $overwrite
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function setHeaders(array $headers, bool $overwrite = FALSE) : void {
		if(! empty($headers)):
			foreach($headers as $name => $value):
				$this->setHeader($name, $value, $overwrite);
			endforeach;
		endif;
	}
	
	/**
	 * @param    string|int    $name     The name of the header.
	 * @param    string        $value    The value of the header.
	 * @param    bool          $overwrite
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function setHeader(string|int $name, string $value, bool $overwrite = FALSE) : void {
		if($overwrite || ! isset($this->_headers[$name])):
			$this->_headers[$name] = $value;
		else:
			$msg = sprintf('Header "%s" already exists.', $name);
			throw new InvalidArgumentException($msg);
		endif;
	}
	
	/**
	 * @param    string|int    $name
	 *
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getHeader(string|int $name) : ?string {
		return $this->_headers[$name] ?? NULL;
	}
	
	/**
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function resetHeaders() : void {
		unset($this->_headers);
	}
	
	/**
	 * @param    string|int    $name    The name of the header to reset.
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function resetHeader(string|int $name) : void {
		unset($this->_headers[$name]);
	}
}
