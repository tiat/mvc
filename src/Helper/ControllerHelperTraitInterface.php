<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Mvc
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Mvc\Helper;

//
use Tiat\Router\Response\ResponseContentInterface;
use Tiat\Standard\Parameters\ParametersPluginInterface;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface ControllerHelperTraitInterface extends ParametersPluginInterface, HeadersHelperTraitInterface, ResponseContentInterface {
	
	/**
	 * Search from all values in the registry.
	 *
	 * @param    string    $name       Parameter name to retrieve
	 * @param    mixed     $default    Default value to return if parameter not found
	 * @param    bool      $wildcard
	 *
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	public function fromAll(string $name, mixed $default, bool $wildcard = FALSE) : mixed;
	
	/**
	 * Get value from server data.
	 *
	 * @param    string    $name
	 * @param    mixed     $default
	 * @param    bool      $wildcard
	 *
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	public function fromHeader(string $name, mixed $default, bool $wildcard = FALSE) : mixed;
	
	/**
	 * Get value from POST data.
	 *
	 * @param    string    $name
	 * @param    mixed     $default
	 * @param    bool      $wildcard
	 *
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	public function fromPost(string $name, mixed $default, bool $wildcard = FALSE) : mixed;
	
	/**
	 * Get value from query string.
	 *
	 * @param    string    $name
	 * @param    mixed     $default
	 * @param    bool      $wildcard
	 *
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	public function fromQuery(string $name, mixed $default, bool $wildcard = FALSE) : mixed;
	
	/**
	 * Get value from cookies.
	 *
	 * @param    string    $name
	 * @param    mixed     $default
	 * @param    bool      $wildcard
	 *
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	public function fromCookie(string $name, mixed $default, bool $wildcard = FALSE) : mixed;
	
	/**
	 * Retrieves a value from uploaded files.
	 *
	 * @param    string    $name        The name of the file input field.
	 * @param    mixed     $default     The default value to return if the file input field does not exist.
	 * @param    bool      $wildcard    Whether to return an array of all uploaded files matching the name.
	 *
	 * @return mixed The value(s) from the uploaded files.
	 * @since   3.0.0 First time introduced.
	 */
	public function fromFiles(string $name, mixed $default, bool $wildcard = FALSE) : mixed;
	
	/**
	 * Sets whether the controller helper should be case-sensitive.
	 *
	 * @param    bool    $value    Whether to enable case sensitivity.
	 *
	 * @return ControllerHelperTraitInterface The updated instance of the controller helper.
	 * @since   3.0.0 First time introduced.
	 */
	public function setCaseSensitive(bool $value) : ControllerHelperTraitInterface;
	
	/**
	 * Redirects the user to the OpenAPI schema endpoint.
	 * This method sets the appropriate HTTP headers and redirects the user to the OpenAPI schema
	 * endpoint. The endpoint URL is constructed using the current server name and the path "/schema/openapi".
	 *
	 * @return void
	 */
	public function forwardToOpenApiSchema() : void;
}
