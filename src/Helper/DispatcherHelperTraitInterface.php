<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Mvc
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Mvc\Helper;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface DispatcherHelperTraitInterface {
	
	/**
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function dispatchStart() : void;
	
	/**
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function preDispatch() : void;
	
	/**
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function postDispatch() : void;
	
	/**
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function dispatchEnd() : void;
}
