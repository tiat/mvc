<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Mvc
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Mvc\Helper;

//
use Jantia\Asi\Interface\AbstractAsi;
use Tiat\Mvc\Exception\RuntimeException;
use Tiat\Standard\DataModel\VariableElement;

use function mb_strtolower;
use function method_exists;
use function str_contains;

/**
 * Trait ControllerHelperTrait
 * Helper methods for getting user input variables by method (query, post, cookie etc.)
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
trait ControllerHelperTrait {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	private const string INDEX_ALL = 'ALL';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	private const string INDEX_USER_INPUT = AbstractAsi::VARIABLES_USER_INPUT;
	
	/**
	 * @var bool
	 * @since   3.0.0 First time introduced.
	 */
	private bool $_caseSensitive = FALSE;
	
	/**
	 * @param    NULL|string    $name    Parameter name to retrieve, or null to get all
	 * @param    null|mixed     $default
	 * @param    bool           $wildcard
	 *
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	public function fromAll(?string $name = NULL, mixed $default = NULL, bool $wildcard = FALSE) : mixed {
		return $this->_getParams(self::INDEX_ALL, $name, $wildcard) ?? $default;
	}
	
	/**
	 * @param    string         $index
	 * @param    NULL|string    $name
	 * @param    bool           $wildcard
	 *
	 * @return null|mixed
	 * @since   3.0.0 First time introduced.
	 */
	private function _getParams(string $index, ?string $name = NULL, bool $wildcard = FALSE) : mixed {
		// Check that ParametersPlugin is available
		if(method_exists($this, 'getParam')):
			// Get all user input parameters
			if(! empty($params = $this->getParam(self::INDEX_USER_INPUT))):
				// Check if ALL variables are requested
				if($index === self::INDEX_ALL):
					$params = $this->_mergeAllVariables($params);
				endif;
				
				//
				if($name !== NULL):
					if($wildcard === TRUE):
						return $this->_searchWildcard($params[$index], $name);
					elseif($this->_caseSensitive !== FALSE):
						return $params[$index][$name] ?? NULL;
					else:
						return $this->_searchComplete($params[$index], $name);
					endif;
				endif;
				
				//
				return $params[$index] ?? NULL;
			endif;
		else:
			$msg = "Unable to retrieve user input parameters because getParam() method is missing";
			throw new RuntimeException($msg);
		endif;
		
		//
		return NULL;
	}
	
	/**
	 * Get all variables from all input sources into one array.
	 *
	 * @param    array    $params
	 *
	 * @return array|array[]
	 * @since   3.0.0 First time introduced.
	 */
	private function _mergeAllVariables(array $params) : array {
		//
		if(! empty($params)):
			foreach($params as $val):
				$result = [...$result ?? [], ...$val];
			endforeach;
		endif;
		
		// Modify the key to INDEX_ALL and return all variables in one array
		return [self::INDEX_ALL => $result] ?? $params;
	}
	
	/**
	 * @param    array     $params
	 * @param    string    $name
	 *
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	private function _searchWildcard(array $params, string $name) : ?array {
		//
		if(! empty($params) && method_exists($this, 'getEncoding')):
			//
			if($this->_caseSensitive === FALSE):
				$name = mb_strtolower($name, $this->getEncoding());
			endif;
			
			//
			foreach($params as $key => $val):
				//
				if($this->_caseSensitive !== FALSE):
					$index = $key;
				else:
					// Search key as case-sensitive
					$index = mb_strtolower($key, $this->getEncoding());
				endif;
				
				//
				if(str_contains($index, $name)):
					$result[$key] = $val;
				endif;
			endforeach;
			
			//
			return $result ?? NULL;
		endif;
		
		//
		return NULL;
	}
	
	/**
	 * This search will perform NOT case-sensitive search
	 *
	 * @param    array     $params
	 * @param    string    $name
	 *
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	private function _searchComplete(array $params, string $name) : mixed {
		if($this->_caseSensitive === FALSE && method_exists($this, 'getEncoding')):
			// Lowercase the name
			$name = mb_strtolower($name, $this->getEncoding());
			
			//
			foreach($params as $key => $val):
				if(mb_strtolower($key, $this->getEncoding()) === $name):
					return $val;
				endif;
			endforeach;
		endif;
		
		//
		return NULL;
	}
	
	/**
	 * @param    NULL|string    $name
	 * @param    null|mixed     $default
	 * @param    bool           $wildcard
	 *
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	public function fromHeader(?string $name = NULL, mixed $default = NULL, bool $wildcard = FALSE) : mixed {
		return $this->_getParams(VariableElement::SERVER->value, $name, $wildcard);
	}
	
	/**
	 * @param    NULL|string    $name
	 * @param    null|mixed     $default
	 * @param    bool           $wildcard
	 *
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	public function fromPost(?string $name = NULL, mixed $default = NULL, bool $wildcard = FALSE) : mixed {
		return $this->_getParams(VariableElement::POST->value, $name, $wildcard);
	}
	
	/**
	 * @param    NULL|string    $name
	 * @param    null|mixed     $default
	 * @param    bool           $wildcard
	 *
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	public function fromQuery(?string $name = NULL, mixed $default = NULL, bool $wildcard = FALSE) : mixed {
		return $this->_getParams(VariableElement::GET->value, $name, $wildcard);
	}
	
	/**
	 * @param    null|string    $name
	 * @param    null|mixed     $default
	 * @param    bool           $wildcard
	 *
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	public function fromCookie(?string $name = NULL, mixed $default = NULL, bool $wildcard = FALSE) : mixed {
		return $this->_getParams(VariableElement::COOKIE->value, $name, $wildcard);
	}
	
	/**
	 * @param    NULL|string    $name
	 * @param    null|mixed     $default
	 * @param    bool           $wildcard
	 *
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	public function fromFiles(?string $name = NULL, mixed $default = NULL, bool $wildcard = FALSE) : mixed {
		// @TODO Implement this method
		return NULL;
	}
	
	/**
	 * @param    bool    $value
	 *
	 * @return ControllerHelperTraitInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setCaseSensitive(bool $value) : ControllerHelperTraitInterface {
		//
		$this->_caseSensitive = $value;
		
		//
		return $this;
	}
}
