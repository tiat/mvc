<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Mvc
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Mvc\Helper;

/**
 * HeadersHelperTraitInterface provides a set of methods to manage headers.
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface HeadersHelperTraitInterface {
	
	/**
	 * Get all headers.
	 *
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function getHeaders() : ?array;
	
	/**
	 * Get a specific header by name.
	 *
	 * @param    string|int    $name
	 *
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getHeader(string|int $name) : ?string;
	
	/**
	 * Set headers en masse.
	 *
	 * @param    array    $headers
	 * @param    bool     $overwrite
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function setHeaders(array $headers, bool $overwrite = FALSE) : void;
	
	/**
	 * Sets a custom header.
	 *
	 * @param    string|int    $name         The name of the header.
	 * @param    string        $value        The value of the header.
	 * @param    bool          $overwrite    Overwrite existing headers.
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function setHeader(string|int $name, string $value, bool $overwrite = FALSE) : void;
	
	/**
	 * Reset the headers.
	 * This function unsets the headers, effectively clearing them.
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function resetHeaders() : void;
	
	/**
	 * Reset a header by name.
	 *
	 * @param    string|int    $name    The name of the header to reset.
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function resetHeader(string|int $name) : void;
}
