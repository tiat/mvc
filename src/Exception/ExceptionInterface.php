<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Mvc
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Mvc\Exception;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface ExceptionInterface {

}
