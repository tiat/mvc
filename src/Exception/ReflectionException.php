<?php

/**
 * Tiat Platform
 *
 * @package        Tiat/Mvc
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Mvc\Exception;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class ReflectionException extends \ReflectionException implements ExceptionInterface {

}
