<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Mvc
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Mvc;

//
use Tiat\Mvc\Controller\ControllerInterface;
use Tiat\Router\Router\MapInterface;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface DispatchManagerInterface {
	
	/**
	 * Has route map set or not. True if set.
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function isRouteMap() : bool;
	
	/**
	 * @return null|MapInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getRouteMap() : ?MapInterface;
	
	/**
	 * @param    MapInterface    $map
	 *
	 * @return DispatchManagerInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setRouteMap(MapInterface $map) : DispatchManagerInterface;
	
	/**
	 * @return DispatchManagerInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetRouteMap() : DispatchManagerInterface;
	
	/**
	 * @return null|ControllerInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getControllerInterface() : ?ControllerInterface;
	
	/**
	 * @param    ControllerInterface    $controller
	 *
	 * @return DispatchManagerInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setControllerInterface(ControllerInterface $controller) : DispatchManagerInterface;
	
	/**
	 * @return DispatchManagerInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetControllerInterface() : DispatchManagerInterface;
	
	/**
	 * Declare controller with route info from setRouteMap())
	 *
	 * @return DispatchManagerInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function declareControllerInterface() : DispatchManagerInterface;
	
	/**
	 * @return DispatchManagerInterface
	 */
	public function createDispatcher() : DispatchManagerInterface;
	
	/**
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function checkApplicationDispatcherPlugin() : ?string;
	
	/**
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getControllerAction() : ?string;
}
