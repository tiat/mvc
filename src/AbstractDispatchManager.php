<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Mvc
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Tiat\Mvc;

//
use Jantia\Asi\Interface\AbstractAsi;
use Jantia\Asi\Interface\Kernel\AbstractAsiKernel;
use Jantia\Asi\Register\AsiRegisterHelper;
use Jantia\Asi\Register\AsiRegisterHelperInterface;
use Tiat\Mvc\Controller\ControllerInterface;
use Tiat\Mvc\Exception\RuntimeException;
use Tiat\Router\Router\MapInterface;
use Tiat\Standard\Loader\ClassLoaderInterface;
use Tiat\Standard\Parameters\ParametersPluginInterface;
use Tiat\Stdlib\Loader\ClassLoaderTrait;

use function sprintf;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
abstract class AbstractDispatchManager implements AsiRegisterHelperInterface, ClassLoaderInterface, DispatchManagerInterface {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	use AsiRegisterHelper;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	use ClassLoaderTrait;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const string DEFAULT_DISPATCHER_APPLICATION = 'ApplicationPlugin\Dispatcher';
	
	/**
	 * @var ControllerInterface
	 * @since   3.0.0 First time introduced.
	 */
	private ControllerInterface $_controllerInterface;
	
	/**
	 * @var DispatcherInterface
	 * @since   3.0.0 First time introduced.
	 */
	private DispatcherInterface $_dispatcherInterface;
	
	/**
	 * @var MapInterface
	 * @since   3.0.0 First time introduced.
	 */
	private MapInterface $_mapInterface;
	
	/**
	 * @param    null|MapInterface    $map
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public function __construct(?MapInterface $map = NULL) {
		//
		if($map !== NULL):
			$this->setRouteMap($map);
		endif;
	}
	
	/**
	 * @param    MapInterface    $map
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setRouteMap(MapInterface $map) : static {
		//
		$this->_mapInterface = $map;
		
		//
		return $this;
	}
	
	/**
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function run(...$args) : static {
		//
		if($this->isInitCompleted() === FALSE):
			$this->init();
		endif;
		
		//
		if($this->isInitCompleted() === TRUE && $this->isRouteMap() === TRUE):
			// Run the application
			$this->getDispatcherInterface()?->run();
		else:
			throw new RuntimeException('Route map has not been set.');
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @param ...$args
	 *
	 * @return ClassLoaderInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function init(...$args) : ClassLoaderInterface {
		//
		if($this->isInitCompleted() === FALSE):
			//
			$this->declareControllerInterface()->createDispatcher();
			
			// Add data for application like user input from GET/POST/...
			if(( $d = $this->getDispatcherInterface()?->setControllerAction($this->getControllerAction()) ) !== NULL &&
			   $d instanceof DispatcherInterface && ( $register = $this->getApplicationRegister() ) !== NULL &&
			   ( $a = $this->getDispatcherInterface()?->getControllerInterface() ) !== NULL):
				// Open Register (if exists)
				$e = $this->getNamedRegister($register[AbstractAsiKernel::INTERFACE_NAME]);
				
				//
				if($e instanceof ParametersPluginInterface && $a instanceof ParametersPluginInterface):
					// Set user variables
					$a->setParam(AbstractAsi::VARIABLES_USER_INPUT, $e->getParam(AbstractAsi::VARIABLES_USER_INPUT));
					
					// Add route map object for application
					$a->setParam('route', $this->getRouteMap());
				endif;
			endif;
			
			//
			$this->setInitCompleted(TRUE);
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	abstract public function createDispatcher() : static;
	
	/**
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function declareControllerInterface() : static {
		//
		if(( $map = $this->getRouteMap() ) !== NULL):
			//
			$class = $map->getNamespaceName() . '\\' . $map->getControllerName();
			
			// Set new application controller
			if(class_exists($class) && is_subclass_of($class, ControllerInterface::class)):
				$this->setControllerInterface(new $class());
			else:
				// If application controller does not exist or is not a subclass of ControllerInterface, reroute to error controller
				$class = $map->getErrorController();
				
				//
				if(class_exists($class) && is_subclass_of($class, ControllerInterface::class)):
					$this->setControllerInterface(new $class());
				else:
					$msg = sprintf("Invalid controller class (%s).", $class);
					throw new RuntimeException($msg);
				endif;
			endif;
		else:
			throw new RuntimeException("No route defined.");
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return null|MapInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getRouteMap() : ?MapInterface {
		return $this->_mapInterface ?? NULL;
	}
	
	/**
	 * @return null|DispatcherInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getDispatcherInterface() : ?DispatcherInterface {
		return $this->_dispatcherInterface ?? NULL;
	}
	
	/**
	 * @param    DispatcherInterface    $dispatcher
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	protected function _setDispatcherInterface(DispatcherInterface $dispatcher) : static {
		//
		$this->_dispatcherInterface = $dispatcher;
		
		//
		return $this;
	}
	
	/**
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getControllerAction() : ?string {
		//
		$map = $this->getRouteMap();
		
		//
		if($map !== NULL):
			return $map->getActionName();
		else:
			throw new RuntimeException("No route defined.");
		endif;
	}
	
	/**
	 * @return null|ControllerInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getControllerInterface() : ?ControllerInterface {
		return $this->_controllerInterface ?? NULL;
	}
	
	/**
	 * @param    ControllerInterface    $controller
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setControllerInterface(ControllerInterface $controller) : static {
		//
		$this->_controllerInterface = $controller;
		
		//
		return $this;
	}
	
	/**
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function isRouteMap() : bool {
		return ! empty($this->_mapInterface);
	}
	
	/**
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function checkApplicationDispatcherPlugin() : ?string {
		// Check that has the application Dispatcher Plugin
		return self::DEFAULT_DISPATCHER_APPLICATION;
	}
	
	/**
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function resetRouteMap() : static {
		//
		unset($this->_mapInterface);
		
		//
		return $this;
	}
	
	/**
	 * @return DispatchManagerInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetControllerInterface() : DispatchManagerInterface {
		//
		unset($this->_controllerInterface);
		
		//
		return $this;
	}
}
